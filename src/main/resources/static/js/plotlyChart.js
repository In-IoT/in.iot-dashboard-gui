function plotlyChart(widget, parentDiv) {

	var parentDiv = document.getElementById('script' + widget.id).parentNode;
	var xx;
	var yy;
	var traces = new Array();
	var event;
	var trace = {};
	


	for (i = 0; i < widget.devices.length; i++) {
		trace = {};

		xx = new Array();
		yy = new Array();
		for (k = 0; k < widget.devices[i].messages.length; k++) {
			event = widget.devices[i].messages[k].events;
			yy.push(event[widget.devices[i].plotVariable]);
			xx.push(widget.devices[i].messages[k].created_at);
		}
		trace.x = xx;
		trace.y = yy;
		trace.name = widget.devices[i].plotVariable;
		trace.type = widget.devices[i].chartType;
		
		if(trace.type =='area'){
			trace.type = 'scatter';
			trace.mode = 'none';
			trace.fill = 'tozeroy';
		}

		traces.push(trace);
		
		
	}

	var xaxis = {};
	yaxis = {};
	xaxis.title = "Date";
	
	if(widget.yName != null)
		yaxis.title=widget.yName;
	if(widget.min != null){
		yaxis.range = [widget.min, widget.max]
	}
		
	
	var layout = {
		//width : widget.width - 10, // or any new width
		height : widget.height - 30
	// " "
	};
	layout.xaxis = xaxis;
	layout.yaxis = yaxis;

	var data = traces;

	//var Plotly;
	Plotly.newPlot('chart' + widget.id, data, layout, {
		scrollZoom : true,
		displaylogo: false,
		responsive: true
	});
	
	
	
	$(parentDiv)
	.resizable(
			{
				stop : function(event, ui) {
					var url = "";
					url = url + window.location.href
							+ "/resizeDiv";
					var clientHeight = document
							.getElementById(parentDiv.id).clientHeight;
					var clientWidth = document
							.getElementById(parentDiv.id).clientWidth;
					var xhr = new XMLHttpRequest();
					xhr.open("POST", url, true);
					xhr.setRequestHeader(
							"Content-Type",
							"application/json");
					var data = JSON.stringify({
						"height" : clientHeight,
						"width" : clientWidth,
						"id" : parentDiv.id
					});
					xhr.send(data);

					//var grapharea = document.getElementById([['canvas' +${widget.id}]]);

					//var myChart = new Chart(grapharea);

					var update = {
							  width: ui.size.width = Math
								.round(ui.size.width / 30) * 30 -10,  // or any new width
							  height: ui.size.height = Math
								.round(ui.size.height / 30) * 30 - 30 // " "
							};

							Plotly.relayout('chart' + widget.id, update);
					
					//Plotly.resize();
					//document.getElementById([['canvas' +${widget.id}]]).setAttribute("style",'width:' + clientWidth + 'px; height:' + clientHeight +'px');
				},
				resize : function(event, ui) {
					console.log("Resize3");
					ui.size.width = Math
							.round(ui.size.width / 30) * 30;
					ui.size.height = Math
							.round(ui.size.height / 30) * 30;
					console.log("Resize4");
				}
			});

};