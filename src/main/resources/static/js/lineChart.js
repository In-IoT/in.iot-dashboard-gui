function lineChart(widget, parentDiv) {  

	//console.log(widget)
	var plotvaluesString = widget.plotValues;
	var max = Math.max(...plotvaluesString);
	var min = Math.min(...plotvaluesString);

	var values = widget.messages;
	


	var parentDiv = document.getElementById('script' +widget.id).parentNode;
	var chart = new Chart(
			document
					.getElementById('canvas' +widget.id),
			{
				type : "line",
				data : {
					labels : widget.labelValues,
					datasets : [ {
						label : widget.variable,
						backgroundColor : "transparent",
						borderColor : "#0088d4",
						data : plotvaluesString
					} ]
				},

				options : {
					tooltips: {
					    callbacks: {
					        afterBody: function(item, data) {
				                //return "$" + Number(tooltipItem.yLabel) + " and so worth it !" + tooltipItem.datasetIndex;
				                //var te = tooltipItem.datasetIndex;
				                //console.log(item);
				                
				                var aux = values[item[0].index].events;
				                var key = widget.variable;
				                delete aux[key];

				                var myJSON = JSON.stringify(aux);
				                //var myJSON = aux.substr(1).slice(0, -1);
								//values.
								myJSON = myJSON.substr(1).slice(0, -1);
								
								myJSON = myJSON.replace(/, /g, ',');
								myJSON = myJSON.replace(/=/g, ':');
								myJSON = myJSON.replace(/"/g, '');
								
								var res3 = myJSON.split(",");
				                return res3;
				                //return test[item[0].index].events;//tooltipItem.datasetIndex + ' uuu ' + tooltipItem.index +'ddd ' + tooltipItem['index'] + data.index;
				            }
					    }
					
//						    callbacks: {
//						        label: function(tooltipItem) {
//						            return values;
//						        }
//						    }
					},
					responsive: true,
				    maintainAspectRatio:false,
					elements : {
						line : {
							tension : 0
						}
					},

					scales : {
						yAxes: [{
					        ticks: {
										reverse: false,
										max: max +1,
										min: min -1	
										}
					      }],
						xAxes: [{
							ticks: {
						          // Make labels vertical
						          // https://stackoverflow.com/questions/28031873/make-x-label-horizontal-in-chartjs

						          // Limit number of labels
						          // https://stackoverflow.com/questions/22064577/limit-labels-number-on-chartjs-line-chart
						          autoSkip: true,
						          maxTicksLimit: 2
						        },
							  type: 'time',
							  position: 'bottom',
							  time: {
							    displayFormats: {'day': 'MMM D'},
							    tooltipFormat: 'DD:MM:YYYY - HH:mm:ss',
							   }
							}]
					},
					
			        // Container for pan options
			        pan: {
			            // Boolean to enable panning
			            enabled: true,

			            // Panning directions. Remove the appropriate direction to disable 
			            // Eg. 'y' would only allow panning in the y direction
			            mode: 'xy'
			        },

			        // Container for zoom options
			        zoom: {
			            // Boolean to enable zooming
			            enabled: true,

			            // Zooming directions. Remove the appropriate direction to disable 
			            // Eg. 'y' would only allow zooming in the y direction
			            mode: 'xy',
			        }
				}
			});
	
	
	$(parentDiv)
	.resizable(
			{
				stop : function(event, ui) {
					var url = "";
					url = url + window.location.href
							+ "/resizeDiv";
					var clientHeight = document
							.getElementById(parentDiv.id).clientHeight;
					var clientWidth = document
							.getElementById(parentDiv.id).clientWidth;
					var xhr = new XMLHttpRequest();
					xhr.open("POST", url, true);
					xhr.setRequestHeader(
							"Content-Type",
							"application/json");
					var data = JSON.stringify({
						"height" : clientHeight,
						"width" : clientWidth,
						"id" : parentDiv.id
					});
					xhr.send(data);

					//var grapharea = document.getElementById([['canvas' +${widget.id}]]);

					//var myChart = new Chart(grapharea);

					chart.resize();
					//document.getElementById([['canvas' +${widget.id}]]).setAttribute("style",'width:' + clientWidth + 'px; height:' + clientHeight +'px');
				},
				resize : function(event, ui) {
					console.log("Resize3");
					ui.size.width = Math
							.round(ui.size.width / 30) * 30;
					ui.size.height = Math
							.round(ui.size.height / 30) * 30;
					console.log("Resize4");
				}
			});
};