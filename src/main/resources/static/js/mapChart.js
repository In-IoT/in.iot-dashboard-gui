function mapChart(widget, parentDiv) {  
	
	
	var event = widget.devices[0].messages[0].events;
	


	var map = L.map('map' + widget.id).setView([event[widget.variablesList[0]],event[widget.variablesList[1]]], 13);
	var myJSON = JSON.stringify(widget.devices[0].messages[0].events);
	console.log(myJSON)
	var values = ''+myJSON;
	values = values.substr(1).slice(0, -1);
	values = values.replace(/=/g, ': ');
	values = values.replace(/,/g, '<br>');
	
	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
	  attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
	}).addTo(map);

	L.marker([event[widget.variablesList[0]],event[widget.variablesList[1]]]).addTo(map)
	 .bindPopup( values + '<br> Received: ' + widget.devices[0].messages[0].created_at.toLocaleString() )
	 .openPopup();		
	
	$(parentDiv)
	.resizable(
			{
				stop : function(event, ui) {
					var url = "";
					url = url + window.location.href
							+ "/resizeDiv";
					var clientHeight = document
							.getElementById(parentDiv.id).clientHeight;
					var clientWidth = document
							.getElementById(parentDiv.id).clientWidth;
					var xhr = new XMLHttpRequest();
					xhr.open("POST", url, true);
					xhr.setRequestHeader(
							"Content-Type",
							"application/json");
					var data = JSON.stringify({
						"height" : clientHeight,
						"width" : clientWidth,
						"id" : parentDiv.id
					});
					//document.getElementById([['map' + ${widget.id}]]).style.height = '100%';
					//document.getElementById([['map' + ${widget.id}]]).style.width = '100%';
					map.invalidateSize();
					xhr.send(data);
					
				},
				resize : function(event, ui) {
					ui.size.width = Math
							.round(ui.size.width / 30) * 30;
					ui.size.height = Math
							.round(ui.size.height / 30) * 30;
				}
			});
};