function lightChart(widget, parentDiv, prefix) {  
	
	var greenIcon = new L.Icon({
		  iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
		  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
		  iconSize: [25, 41],
		  iconAnchor: [12, 41],
		  popupAnchor: [1, -34],
		  shadowSize: [41, 41]
		});
	
	var redIcon = new L.Icon({
		  iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
		  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
		  iconSize: [25, 41],
		  iconAnchor: [12, 41],
		  popupAnchor: [1, -34],
		  shadowSize: [41, 41]
		});
	
	
	var blackIcon = new L.Icon({
		  iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-black.png',
		  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
		  iconSize: [25, 41],
		  iconAnchor: [12, 41],
		  popupAnchor: [1, -34],
		  shadowSize: [41, 41]
		});

	
	console.log(widget);
	var event = widget;
	
	
	var i;
	var map = L.map('map' + widget.id).setView([widget.latitudeCenter,widget.longitudeCenter], 13);
	
	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
		  attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(map);
	for (i = 0; i < event.devices.length; i++) {

		L.marker([event.devices[i].latitude,event.devices[i].longitude], {icon: blackIcon}).addTo(map)
		 .bindPopup( '<br/><button type="button" class="btn btn-primary sidebar-open-button" onclick="test()" >Click for more</button>' )
		 ;

	}
	


	
			
	
	$(parentDiv)
	.resizable(
			{
				stop : function(event, ui) {
					var url = "";
					url = url + window.location.href
							+ "/resizeDiv";
					var clientHeight = document
							.getElementById(parentDiv.id).clientHeight;
					var clientWidth = document
							.getElementById(parentDiv.id).clientWidth;
					var xhr = new XMLHttpRequest();
					xhr.open("POST", url, true);
					xhr.setRequestHeader(
							"Content-Type",
							"application/json");
					var data = JSON.stringify({
						"height" : clientHeight,
						"width" : clientWidth,
						"id" : parentDiv.id
					});
					//document.getElementById([['map' + ${widget.id}]]).style.height = '100%';
					//document.getElementById([['map' + ${widget.id}]]).style.width = '100%';
					map.invalidateSize();
					xhr.send(data);
					
				},
				resize : function(event, ui) {
					ui.size.width = Math
							.round(ui.size.width / 30) * 30;
					ui.size.height = Math
							.round(ui.size.height / 30) * 30;
				}
			});
};

function test(){
	
	console.log('oh boy')
}