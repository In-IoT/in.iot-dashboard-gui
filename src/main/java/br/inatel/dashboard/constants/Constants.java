package br.inatel.dashboard.constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Constants {
	
	public static final String PREFIX;//="/dashboard-api";
	public static final String MONGOURL; //= "mongodb://localhost:27017";
    
    public static final String MONGODATABASE; //= "springmongodb";

	static {
		InputStream inputStream = null;
		Properties prop = new Properties();
		String propFileName = "application.properties";
		
		try {
			inputStream = new FileInputStream (System.getProperty("user.dir")+ File.separator + "config" + File.separator + "application.properties");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
    	String prefixAux = prop.getProperty("eureka.client.register-with-eureka", "true");
    	if(prefixAux.equals("true")) {
    		PREFIX = "/admin-gui";
    	} else {
    		PREFIX = "";
    	}

		
    	MONGOURL = prop.getProperty("MONGODBURL", "mongodb://localhost:27017");
    	MONGODATABASE = prop.getProperty("MONGODATABASE", "iniot");
    	
    }
	
	public static final String SIGN_UP_URL_FIRST_USER = "/signup";
	

	public static final String SIGN_UP_URL_ADDITIONAL_USER = "/newUsers";
	
	public static final String UPLOAD_URL = "/upload";

	public static final String NEW_DEVICES_URL = "/newDevices";

	public static final String SIGN_IN_URL = "/auth/login";

	public static final String LIST_USERS_URL = "/users";

	public static final String LIST_DEVICES_URL = "/devices";

	public static final String DASHBOARD_URL = "/dashboard";
	
	public static final String PUBLIC_DASHBOARD_URL = "/publicDashboard";

	public static final String NEW_DASHBOARD_URL = "/newDashboard";
	
	public static final String DELETE_DASHBOARD_URL = "/deleteDashboard";

	public static final String NEW_WIDGET_URL = "/newWidget";

	public static final String LOGOUT_URL = "/signoff";

	public static final String LOGIN_PROCESSING_URL = "/login";

	public static final String AUTO_REGISTER_URL = "/configureAutoregisterDevice";
	
	public static final String DASHBOARD_HEADER_AUTH= "/api/v1/message/headerAuth";
	
	public static final String DASHBOARD_BODY_AUTH = "/api/v1/message/bodyAuth";
		
	// DASHBOARD GUI ///
	
	public static final String DASHBOARD_LOGIN_URL = PREFIX+"/dashboard";
	
	public static final String DASHBOARD_NEW_DEVICES_URL = PREFIX+"/newDevices";
	
	public static final String DASHBOARD_NEW_USERS_URL = PREFIX+"/newUsers";
	
	public static final String DASHBOARD_AUTO_REGISTER_URL = PREFIX+"/configureAutoregisterDevice";
	
	public static final String DASHBOARD_IN_IOT_LOGO_URL = PREFIX+"/images/Fwd__Logo_In._IoT/Logo.png";

	public static final String DASHBOARD_LIST_DEVICES_URL = PREFIX+"/devices";
	
	public static final String DASHBOARD_LIST_USERS_URL = PREFIX+"/users";
	
	public static final String DASHBOARD_NEW_DASHBOARD_URL = PREFIX + "/newDashboard";
	
	public static final String DASHBOARD_SIGNOFF_URL = PREFIX + "/signoff";
	
	public static final String DASHBOARD_NEW_WIDGET_URL = PREFIX + "/newWidget";
	
	public static final String DASHBOARD_SIGNUP_URL = PREFIX + "/signup";
	
	
	// ADMIN REST ///
	
	public static final String LIST_DEVICES_REST_URL = "/listDevices";
	
	public static final String LIST_DEVICE_EVENTS_REST_URL = "/listDeviceEvents";
	
	public static final String AUTO_REGISTER_URL_REST = "/configureAutoregisterDeviceREST";

	public static final String SIGN_UP_URL_FIRST_USER_REST = "/signupUser";

	public static final String SIGN_UP_URL_ADDITIONAL_USER_REST = "/newUsersREST";
	
	public static final String GET_DASHBOARDS_FROM_USER_REST_URL = "/getDashboardsFromUser";
	
	public static final String NEW_DEVICES_REST_URL = "/newDevicesREST";
	
	public static final String NEW_DASHBOARD_REST_URL = "/newDashboardREST";
	
	public static final String NEW_WIDGET_REST_URL = "/newWidgetREST";
	
	public static final String DASHBOARD_REST_URL = "/dashboardREST";
	
	public static final String VERIFY_JWT = "/getUserDetails";
	
	public static final String LIST_ACTIVE_MQTT_DEVICES_URL = "/listMQTTDevices";
	
	public static final String EDIT_DEVICES_URL = "/edit";

}
