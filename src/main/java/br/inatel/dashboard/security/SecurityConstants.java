package br.inatel.dashboard.security;

public class SecurityConstants {

    /*
        * Security settings
     */
    public static final String SECRETUSER = "SecretKeyToGenJWTsUSER";
    public static final String SECRETDEVICE = "SecretKeyToGenJWTsDEVICE123";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String HEADER_STRING = "Authorization";

    /*
        * Links Signin
        * Class - JWTAuthenticationFilterUser
        *       - JWTAuthenticationFilterDevice
     */
    public static final String SIGN_IN_URL_DEVICES = "/api/v1/auth/device/signin";
    public static final String LOGIN_GUI = "/loginPage";

    /*
        * Links Signup
        * Class - WebSecurity
     */
    public static final String SIGN_UP_URL_DEVICES = "/api/v1/auth/device/signup";

    /*
        * Links Controller
        * Class do pacote controller
     */
    public static final String CONTROLLER_URL_DEVICE = "/api/v1/devices";
    public static final String CONTROLLER_URL_MESSAGE = "/api/v1/message";
    public static final String CONTROLLER_AUTH_URL_DEVICE = "/api/v1/auth/device";
    
}
