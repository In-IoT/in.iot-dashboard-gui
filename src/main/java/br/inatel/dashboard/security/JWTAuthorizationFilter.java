package br.inatel.dashboard.security;

import static br.inatel.dashboard.constants.Constants.PREFIX;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import br.inatel.dashboard.Feign.Interface.AdminAPIProxyEureka;
import br.inatel.dashboard.Feign.Interface.AdminAPIProxyNoEureka;
import feign.FeignException;

public class JWTAuthorizationFilter extends OncePerRequestFilter {
//public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	//@Autowired
//	public JWTAuthorizationFilter(AuthenticationManager authManager) {
//		super(authManager);
//	}
//	
	
	
	
	@Autowired
	AdminAPIProxyEureka adminAproxy;
	
	@Autowired
	AdminAPIProxyNoEureka adminAproxy2;

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		String URI = req.getRequestURI();
		int lengthURI = URI.length();
		if (lengthURI < 3) {
		} else if (URI.charAt(lengthURI - 3) == '.' || URI.charAt(lengthURI - 4) == '.') {
			chain.doFilter(req, res);
			return;
		}
		// if(req.getRequestURI() =="loginPage")
		// return;

		String token = CookieUtil.getValue(req, "JWT-TOKEN") + ""; // to guarantee that the token is never null
	
		UsernamePasswordAuthenticationToken authentication = getAuthentication(token);

		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(req, res);

	}

	private UsernamePasswordAuthenticationToken getAuthentication(String token) {


		if (token.length() > 125) {

			ResponseEntity<String> response = null;// new ResponseEntity<>("Hello World!", HttpStatus.OK);// = new ResponseEntity<>;
			//adminAproxy.isJWTValid(token);
			//adminAproxy2.isJWTValid(token);
			 try {
				 if(PREFIX.length()>1)
						response = adminAproxy.isJWTValid(token);
					else
						response = adminAproxy2.isJWTValid(token);
		            //doSomething();
		        } catch (NullPointerException | FeignException e) {
		            //System.out.print("Caught the NullPointerException or the FeignException");
		        }
			
			//response = adminAproxy.isJWTValid(token);
			
			// if(adminAproxy.isJWTValid(token).getStatusCodeValue() != 403)
			 if(response == null)
				 return null;
			if (response.getStatusCodeValue() == 403)
				return null;
			

			JSONObject jsonObj = new JSONObject(response.getBody());

			String user = jsonObj.getString("email");
			String application = jsonObj.getString("application");

			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
					user, null, new ArrayList<>());

			usernamePasswordAuthenticationToken.setDetails(application);
			return usernamePasswordAuthenticationToken;
		}
		return null;

	}
}
