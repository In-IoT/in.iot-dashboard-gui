package br.inatel.dashboard.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception to handle user already exists in the database.
 */
@ResponseStatus(value = HttpStatus.CONFLICT)
public class DeviceAlreadyExistsException extends RuntimeException {

    private static final long serialVersionUID = -3709703815502458804L;

    public DeviceAlreadyExistsException(String message) {
        super(message);
    }
}
