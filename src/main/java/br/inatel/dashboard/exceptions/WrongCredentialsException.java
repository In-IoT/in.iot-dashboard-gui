package br.inatel.dashboard.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)

public class WrongCredentialsException extends RuntimeException {

    private static final long serialVersionUID = 2048809100291806394L;

    public WrongCredentialsException(String message) {
        super(message);
    }
}
