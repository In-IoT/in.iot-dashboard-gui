package br.inatel.dashboard.model;


import java.util.List;



public class MQTTDeviceList {

	private List<MQTTDevice> users;
	
	public MQTTDeviceList() {
	}


	public MQTTDeviceList(List<MQTTDevice> users) {
		this.users = users;
	}
	

	public List<MQTTDevice> getUsers() {
		return users;
	}

	public void setUsers(List<MQTTDevice> users) {
		this.users = users;
	}
	
	
	
	
}
