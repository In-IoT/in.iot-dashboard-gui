package br.inatel.dashboard.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Document(collection = "widget")
@JsonInclude(value=Include.NON_NULL)
public class Widget {

	@Id
    private String id;
    private String name;
    private List <String> typeList;
    private List <String> deviceList;
    private String variable;
    private String username;
    private String publicUsername;
    private String height;
    private String width;
    private List <String> variablesList;
    private String latitude;
    private String longitude;
    private List<Message> messages;// = new ArrayList<Message>();
    private List<Device> devices;// = new ArrayList<Message>();
    private String dashboardId;
    private String idWidget;
    
    private Double min;
    private Double max;
    private String yName;
    private Integer maxResults;
    
    private String group;
    private String latitudeCenter;
    private String longitudeCenter;
    
    
    public String getIdWidget() {
		return idWidget;
	}

	public void setIdWidget(String idWidget) {
		this.idWidget = idWidget;
	}

	@DBRef
    private Dashboard dashboard;
    
    List<String> labelValues;// = new ArrayList<String>();
	List<Double> plotValues;// = new ArrayList<Double>();
	
	
    public Widget(
            @JsonProperty("id") String id,
            @JsonProperty("username") String username,
            @JsonProperty("height") String height,
            @JsonProperty("width") String width,
            @JsonProperty("dashboard") Dashboard dashboard,
            @JsonProperty("deviceList") List <String> deviceList,
            @JsonProperty("variable") String variable,
            @JsonProperty("name") String name,
            @JsonProperty("typeList") List <String> typeList,
            @JsonProperty("dashboardId") String dashboardId,
            @JsonProperty("idWdiget") String idWidget,
            @JsonProperty("variablesList") List<String> variablesList
    ) {
        this.id = id;
        this.username = username;
        this.height = height;
        this.width = width;
        this.dashboard = dashboard;
        this.deviceList = deviceList;
        this.variable = variable;
        this.name = name;
        this.typeList = typeList;
        this.variablesList = variablesList;
        this.dashboardId = dashboardId;
        this.idWidget = idWidget;
    }

    public Widget() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Dashboard getDashboard() {
		return dashboard;
	}

	public void setDashboard(Dashboard dashboard) {
		this.dashboard = dashboard;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}



	public List<String> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<String> typeList) {
		this.typeList = typeList;
	}

	public List<String> getLabelValues() {
		return labelValues;
	}

	public void setLabelValues(List<String> labelValues) {
		this.labelValues = labelValues;
	}

	public List<Double> getPlotValues() {
		return plotValues;
	}

	public void setPlotValues(List<Double> plotValues) {
		this.plotValues = plotValues;
	}

	public List<String> getVariablesList() {
		return variablesList;
	}

	public void setVariablesList(List<String> variablesList) {
		this.variablesList = variablesList;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}


	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public String getDashboardId() {
		return dashboardId;
	}

	public void setDashboardId(String dashboardId) {
		this.dashboardId = dashboardId;
	}

	public List<String> getDeviceList() {
		return deviceList;
	}

	public void setDeviceList(List<String> deviceList) {
		this.deviceList = deviceList;
	}

	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	public String getPublicUsername() {
		return publicUsername;
	}

	public void setPublicUsername(String publicUsername) {
		this.publicUsername = publicUsername;
	}

	public Double getMin() {
		return min;
	}

	public void setMin(Double min) {
		this.min = min;
	}

	public Double getMax() {
		return max;
	}

	public void setMax(Double max) {
		this.max = max;
	}


	public Integer getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}

	public String getyName() {
		return yName;
	}

	public void setyName(String yName) {
		this.yName = yName;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getLatitudeCenter() {
		return latitudeCenter;
	}

	public void setLatitudeCenter(String latitudeCenter) {
		this.latitudeCenter = latitudeCenter;
	}

	public String getLongitudeCenter() {
		return longitudeCenter;
	}

	public void setLongitudeCenter(String longitudeCenter) {
		this.longitudeCenter = longitudeCenter;
	}



}