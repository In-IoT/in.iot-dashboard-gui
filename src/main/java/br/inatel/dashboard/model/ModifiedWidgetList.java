package br.inatel.dashboard.model;

import java.util.List;

public class ModifiedWidgetList {

	private List<Widget> documents;

	public List<Widget> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Widget> documents) {
		this.documents = documents;
	}
	
	
	
}
