package br.inatel.dashboard.model;

public class Event {

    private String var;
    private String value;

    public String getVar() {
        return var;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Event() {

    }

    public Event(String var, String value) {
        this.var = var;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Event [var=" + var + ", value=" + value + "]";
    }

}
