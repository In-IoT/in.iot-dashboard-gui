package br.inatel.dashboard.model;

public class ControlLampEntity {
	
	private String val;
	private String thdL;
	private String thdH;
	private String mode;
	private String dimm;
	
	
	
	public ControlLampEntity(String val, String thdL, String thdH, String mode, String dimm) {
		super();
		this.val = val;
		this.thdL = thdL;
		this.thdH = thdH;
		this.mode = mode;
		this.dimm = dimm;
	}
	public ControlLampEntity() {
		super();
	}
	public String getVal() {
		return val;
	}
	public void setVal(String val) {
		this.val = val;
	}
	public String getThdL() {
		return thdL;
	}
	public void setThdL(String thdL) {
		this.thdL = thdL;
	}
	public String getThdH() {
		return thdH;
	}
	public void setThdH(String thdH) {
		this.thdH = thdH;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getDimm() {
		return dimm;
	}
	public void setDimm(String dimm) {
		this.dimm = dimm;
	}
	
	

}
