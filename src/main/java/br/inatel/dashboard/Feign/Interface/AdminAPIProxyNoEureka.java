package br.inatel.dashboard.Feign.Interface;

import static br.inatel.dashboard.constants.Constants.DASHBOARD_URL;
import static br.inatel.dashboard.constants.Constants.DELETE_DASHBOARD_URL;
import static br.inatel.dashboard.constants.Constants.NEW_WIDGET_URL;

import java.io.File;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

//import br.inatel.dashboard.Feign.Interface.Files.MultipartSupportConfig;
//import br.inatel.dashboard.Feign.Interface.Files.NormalConfig;
import br.inatel.dashboard.model.AutoRegister;
import br.inatel.dashboard.model.Credentials;
import br.inatel.dashboard.model.Dashboard;
import br.inatel.dashboard.model.DashboardList;
import br.inatel.dashboard.model.Device;
import br.inatel.dashboard.model.DeviceList;
import br.inatel.dashboard.model.User;
import br.inatel.dashboard.model.UserList;
import br.inatel.dashboard.model.Widget;
import br.inatel.dashboard.model.WidgetList;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

//@FeignClient(name="admin-api")//, url="localhost:8100")
@FeignClient(name="admin-api",url="localhost:8190")
//@RibbonClient(name="admin-api")
public interface AdminAPIProxyNoEureka {

	
    @RequestMapping(method = RequestMethod.POST, value = "/signupUser", consumes = "application/json")
    User createNewUser(User user);
    
    @RequestMapping(method = RequestMethod.POST, value = "/login", consumes = "application/json")
    ResponseEntity<String> login(Credentials credential);
    
    @RequestMapping(method = RequestMethod.GET, value = "/getDashboardsFromUser", consumes = "application/json")
    DashboardList getDashboardsFromuser(@RequestHeader("Authorization") String token);
    
    @RequestMapping(method = RequestMethod.GET, value = "/isJWTValid",  consumes = "application/json")
    ResponseEntity<String> isJWTValid(@RequestHeader("Authorization") String token);
    
    @RequestMapping(method = RequestMethod.GET, value = "/listOtherUsers",  consumes = "application/json")
    UserList listOtherUsers(@RequestHeader("Authorization") String token);

    @RequestMapping(method = RequestMethod.POST, value = "/newWidgetREST" + "/{dashboardId}",  consumes = "application/json")
    Widget addWidget(@RequestHeader("Authorization") String token, Widget widget, @PathVariable("dashboardId") String dashboardId);

    @RequestMapping(method = RequestMethod.GET, value = "/dashboardData" + "/{dashboardId}",  consumes = "application/json")
    WidgetList getDashboardDetails(@RequestHeader("Authorization") String token, @PathVariable("dashboardId") String dashboardId);

    @RequestMapping(method = RequestMethod.POST, value = DASHBOARD_URL+"/{dashboardId}"+"/resizeDiv",  consumes = "application/json")
    Widget resizeDiv(@RequestHeader("Authorization") String token, Widget widget, @PathVariable("dashboardId") String dashboardId);
    
    @RequestMapping(method = RequestMethod.POST, value = "/newUsersREST",  consumes = "application/json")
    User createUserWhenLogged(@RequestHeader("Authorization") String token, User user);
   
    @RequestMapping(method = RequestMethod.POST, value = "/newDevicesREST",  consumes = "application/json")
    Device createnewDevice(@RequestHeader("Authorization") String token, Device device);
    
    @RequestMapping(method = RequestMethod.POST, value = "/configureAutoregisterDeviceREST",  consumes = "application/json")
    AutoRegister configureAutoRegister(@RequestHeader("Authorization") String token, AutoRegister autoRegister);
    
    @RequestMapping(method = RequestMethod.POST, value = "/newDashboardREST",  consumes = "application/json")
    Dashboard newDashboard(@RequestHeader("Authorization") String token, Dashboard dashboard);
    
    @RequestMapping(method = RequestMethod.GET, value = "/listDevices",  consumes = "application/json")
    DeviceList listDevices(@RequestHeader("Authorization") String token);

    @RequestMapping(method = RequestMethod.POST, value = "/listDevices",  consumes = "application/json")
    Device resetDevicePassword(@RequestHeader("Authorization") String token, Device device);
    
    @RequestMapping(method = RequestMethod.POST, value = DASHBOARD_URL+"/{dashboardId}"+"/removeDiv",  consumes = "application/json")
    void removeDiv(@RequestHeader("Authorization") String token, Widget widget, @PathVariable("dashboardId") String dashboardId);
    
    @RequestMapping(method = RequestMethod.POST, value = NEW_WIDGET_URL+"/{dashboardId}/comboboxChange",  consumes = "application/json")
    List<String> comboboxChange(@RequestHeader("Authorization") String token, Device device, @PathVariable("dashboardId") String widgetId);
    
    @RequestMapping(method = RequestMethod.GET, value = "/publicDashboardData" + "/{dashboardId}",  consumes = "application/json")
    WidgetList getPublicDashboardDetails(@PathVariable("dashboardId") String dashboardId);
    
    @RequestMapping(method = RequestMethod.GET, value = DELETE_DASHBOARD_URL + "/{dashboardId}",  consumes = "application/json")
    WidgetList deleteDashboard(@RequestHeader("Authorization") String token, @PathVariable("dashboardId") String dashboardId);
    
//    @RequestMapping(method = RequestMethod.POST, value = "/listDevices",  consumes = "multipart/form-data")
//    void uploadFile(@RequestHeader("Authorization") String token, @Param("file") MultipartFile file);
    
    @RequestMapping(method = RequestMethod.GET, value = "/findeviceById"+"/{deviceId}", consumes = "application/json")
	Device getDeviceDetails(@RequestHeader("Authorization") String token,
			@PathVariable("deviceId") String deviceId);

}