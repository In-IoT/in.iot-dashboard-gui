//package br.inatel.dashboard.Feign.Interface.Files;
//
//import java.util.Collection;
//
//import org.springframework.beans.factory.ObjectFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
//import org.springframework.cloud.openfeign.support.ResponseEntityDecoder;
//import org.springframework.cloud.openfeign.support.SpringDecoder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Primary;
//import org.springframework.context.annotation.Scope;
//import org.springframework.util.StreamUtils;
//import org.springframework.web.multipart.MultipartFile;
//
//import feign.codec.Decoder;
//import feign.codec.Encoder;
//import feign.form.spring.SpringFormEncoder;
//
//public class MultipartSupportConfig {
//
//	@Autowired
//	ObjectFactory<HttpMessageConverters> messageConverters;
//
//	@Bean
//	@Primary
//	@Scope("prototype")
//	public Encoder feignFormEncoder() {
//		//System.out.println(2);
//		return new SpringFormEncoder();
//	}
//
//	@Bean
//	@Primary
//	@Scope("prototype")
//	public Decoder decoder() {
//		Decoder decoder = (response, type) -> {
//			System.out.println(1);
//			if (type instanceof Class && MultipartFile.class.isAssignableFrom((Class) type)) {
//				System.out.println(2);
//				Collection<String> contentTypes = response.headers().get("content-type");
//				System.out.println(3);
//				String contentType = "application/octet-stream";
//				System.out.println(4);
//				if (contentTypes.size() > 0) {
//					System.out.println(5);
//					String[] temp = new String[contentTypes.size()];
//					contentTypes.toArray(temp);
//					contentType = temp[0];
//				}
//
//				System.out.println(6);
//				byte[] bytes = StreamUtils.copyToByteArray(response.body().asInputStream());
//				InMemoryMultipartFile inMemoryMultipartFile = new InMemoryMultipartFile("file", "", contentType,
//						bytes);
//				System.out.println(7);
//				return inMemoryMultipartFile;
//			}
//			System.out.println(8);
//			return new SpringDecoder(messageConverters).decode(response, type);
//		};
//		System.out.println(9);
//		return new ResponseEntityDecoder(decoder);
//	}
//}