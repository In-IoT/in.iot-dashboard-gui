//package br.inatel.dashboard.Feign.Interface.Files;
//
//import java.util.Collection;
//
//import org.springframework.beans.factory.ObjectFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.cloud.openfeign.support.ResponseEntityDecoder;
//import org.springframework.cloud.openfeign.support.SpringDecoder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.context.annotation.Scope;
//import org.springframework.http.MediaType;
//import org.springframework.util.StreamUtils;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestHeader;
//import org.springframework.web.bind.annotation.RequestPart;
//import org.springframework.web.multipart.MultipartFile;
//
//import feign.codec.Decoder;
//import feign.codec.Encoder;
//import feign.form.spring.SpringFormEncoder;
//
//@FeignClient(name = "admin-api", configuration = { AdminAPIProxyEurekaFiles.MultipartSupportConfig.class })
////@RibbonClient(name="admin-api")
//@Configuration
//public interface AdminAPIProxyEurekaFiles {
//
//	@PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
//	String processFileTest(@RequestHeader("Authorization") String token,
//			@RequestPart(value = "file", required = true) MultipartFile file);
//
//	@GetMapping(value = "/uploadStatus", consumes = "application/octet-stream")
//	MultipartFile downloadFile(@RequestHeader("Authorization") String token);
//
//	public class MultipartSupportConfig {
//
//		@Autowired
//		ObjectFactory<HttpMessageConverters> messageConverters;
//
//		@Bean
//		@Primary
//		@Scope("prototype")
//		public Encoder feignFormEncoder() {
//			return new SpringFormEncoder();
//		}
//
//		@Bean
//		@Primary
//		@Scope("prototype")
//		public Decoder decoder() {
//			Decoder decoder = (response, type) -> {
//				if (type instanceof Class && MultipartFile.class.isAssignableFrom((Class) type)) {
//					Collection<String> contentTypes = response.headers().get("content-type");
//					String contentType = "application/octet-stream";
//					if (contentTypes.size() > 0) {
//						String[] temp = new String[contentTypes.size()];
//						contentTypes.toArray(temp);
//						contentType = temp[0];
//					}
//
//					byte[] bytes = StreamUtils.copyToByteArray(response.body().asInputStream());
//					InMemoryMultipartFile inMemoryMultipartFile = new InMemoryMultipartFile("file", "", contentType,
//							bytes);
//					return inMemoryMultipartFile;
//				}
//				return new SpringDecoder(messageConverters).decode(response, type);
//			};
//			return new ResponseEntityDecoder(decoder);
//		}
//	}
//
//}