//package br.inatel.dashboard.Feign.Interface.Files;
//
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.http.MediaType;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestHeader;
//import org.springframework.web.bind.annotation.RequestPart;
//import org.springframework.web.multipart.MultipartFile;
//
////@FeignClient(name="admin-api")//, url="localhost:8100")
//@FeignClient(name="admin-api",url="localhost:8190", configuration = {MultipartSupportConfig.class})
////@RibbonClient(name="admin-api")
////@Configuration
//public interface AdminAPIProxyNoEurekaFiles {
//
//	@PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
//	String processFileTest(@RequestHeader("Authorization") String token,
//			@RequestPart(value = "file", required = true) MultipartFile file);
//
//	@GetMapping(value = "/uploadStatus", consumes = "application/octet-stream")
//	MultipartFile downloadFile(@RequestHeader("Authorization") String token);
//
////	public class MultipartSupportConfig {
////
////		@Autowired
////		ObjectFactory<HttpMessageConverters> messageConverters;
////
////		@Bean
////		@Primary
////		@Scope("prototype")
////		public Encoder feignFormEncoder() {
////			return new SpringFormEncoder();
////		}
////
////		@Bean
////		@Primary
////		@Scope("prototype")
////		public Decoder decoder() {
////			Decoder decoder = (response, type) -> {
////				if (type instanceof Class && MultipartFile.class.isAssignableFrom((Class) type)) {
////					Collection<String> contentTypes = response.headers().get("content-type");
////					String contentType = "application/octet-stream";
////					if (contentTypes.size() > 0) {
////						String[] temp = new String[contentTypes.size()];
////						contentTypes.toArray(temp);
////						contentType = temp[0];
////					}
////
////					byte[] bytes = StreamUtils.copyToByteArray(response.body().asInputStream());
////					InMemoryMultipartFile inMemoryMultipartFile = new InMemoryMultipartFile("file", "", contentType,
////							bytes);
////					return inMemoryMultipartFile;
////				}
////				return new SpringDecoder(messageConverters).decode(response, type);
////			};
////			return new ResponseEntityDecoder(decoder);
////		}
////	}
//    
//
//}