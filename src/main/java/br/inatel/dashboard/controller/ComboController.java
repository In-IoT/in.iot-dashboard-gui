package br.inatel.dashboard.controller;

import static br.inatel.dashboard.constants.Constants.NEW_WIDGET_URL;
import static br.inatel.dashboard.constants.Constants.PREFIX;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.inatel.dashboard.Feign.Interface.AdminAPIProxyEureka;
import br.inatel.dashboard.Feign.Interface.AdminAPIProxyNoEureka;
import br.inatel.dashboard.model.Device;



@RestController
public class ComboController {
	
	@Autowired
	private AdminAPIProxyEureka adminAPIProxy;
	
	@Autowired
	private AdminAPIProxyNoEureka adminAPIProxy2;

	@RequestMapping(value = NEW_WIDGET_URL+"/{widgetId}/comboboxChange", method = RequestMethod.POST, produces = "application/json")
	public List<String> comboboxChange(@RequestBody Device device,
			@RequestHeader("Cookie") String token, @PathVariable("widgetId") String widgetId) {
		
		token = token.substring(10);

		List<String> stringList;
		device.setUsername(device.getId());
		if(PREFIX.length()>1)
			stringList = adminAPIProxy.comboboxChange(token, device, widgetId);
		else
			stringList = adminAPIProxy2.comboboxChange(token, device, widgetId);
 
		return stringList;


	}
	
	
}