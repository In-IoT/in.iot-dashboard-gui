package br.inatel.dashboard.controller.utils;

import static br.inatel.dashboard.constants.Constants.DASHBOARD_URL;
import static br.inatel.dashboard.constants.Constants.PREFIX;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.inatel.dashboard.Feign.Interface.AdminAPIProxyEureka;
import br.inatel.dashboard.Feign.Interface.AdminAPIProxyNoEureka;
import br.inatel.dashboard.model.Dashboard;
import br.inatel.dashboard.model.Widget;

@RestController
public class UtilsController {
	
	
	@Autowired
	private AdminAPIProxyEureka adminAPIProxy;
	
	@Autowired
	private AdminAPIProxyNoEureka adminAPIProxy2;
	


	@Autowired 
	public UtilsController() {
		

	}
	
	@RequestMapping(value = DASHBOARD_URL+"/{dashboardId}"+"/resizeDiv", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void resizeDiv(@RequestBody Widget widget,
			@PathVariable(name = "dashboardId", required = true) String dashboardId, 
			@RequestHeader("Cookie") String token) {
		
		token = token.substring(10);
        
        
        if(PREFIX.length()>1)
			adminAPIProxy.resizeDiv(token, widget, dashboardId);
		else
			adminAPIProxy2.resizeDiv(token, widget, dashboardId);


	}
	
	@RequestMapping(value = DASHBOARD_URL+"/{dashboardId}"+"/removeDiv", method = RequestMethod.POST)
	public void removeDiv(@RequestBody Widget widget,
			@PathVariable(name = "dashboardId", required = true) String dashboardId,
			@RequestHeader("Cookie") String token) {
		

		token = token.substring(10);
        if(PREFIX.length()>1)
			adminAPIProxy.removeDiv(token, widget, dashboardId);
		else
			adminAPIProxy2.removeDiv(token, widget, dashboardId);
	}

}
