package br.inatel.dashboard.controller;


import static br.inatel.dashboard.constants.Constants.AUTO_REGISTER_URL;
import static br.inatel.dashboard.constants.Constants.PREFIX;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.inatel.dashboard.Feign.Interface.AdminAPIProxyEureka;
import br.inatel.dashboard.Feign.Interface.AdminAPIProxyNoEureka;
import br.inatel.dashboard.model.AutoRegister;
import br.inatel.dashboard.model.DashboardList;


@Controller
public class AutoRegisterController {
	
	@Autowired
	private AdminAPIProxyEureka adminAPIProxy;
	
	@Autowired
	private AdminAPIProxyNoEureka adminAPIProxy2;

    @RequestMapping(value = AUTO_REGISTER_URL, method = RequestMethod.GET)
    public String autoRegisterPage(Model model, HttpServletRequest req) {
    	  
    	String token = req.getHeader("Cookie").substring(10);
    	
    	//DashboardList dashboardList = adminAPIProxy.getDashboardsFromuser(req.getHeader("Cookie").substring(10));

    	DashboardList dashboardList;
		if(PREFIX.length()>1)
			dashboardList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashboardList = adminAPIProxy2.getDashboardsFromuser(token);

		model.addAttribute("dashboards", dashboardList.getDashboards());
        return "configureAutoRegister";
    }

    @RequestMapping(value = AUTO_REGISTER_URL, method = RequestMethod.POST)
    public String autoRegisterConfig(@Valid AutoRegister autoRegister, Model model, HttpServletRequest req) {

        if (!autoRegister.getUsername().trim().equals("") && !autoRegister.getPassword().trim().equals("")) {

        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            //Date expirationDate = null;
//            try {
//                expirationDate = sdf.parse(request.getParameter("inputDate"));
//            } catch (ParseException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
    		
//            autoRegister.setExpirationDate(expirationDate);
        	
        	String token = req.getHeader("Cookie").substring(10);
    		
    		if(PREFIX.length()>1)
    			autoRegister = adminAPIProxy.configureAutoRegister(token, autoRegister);
    		else
    			autoRegister = adminAPIProxy2.configureAutoRegister(token, autoRegister);
    		
        	
//        	String address = "http://" + req.getLocalAddr() + ":" + req.getLocalPort() + AUTO_REGISTER_URL_REST;
//    		Gson gson = new Gson();
//
//    		// 2. Java object to JSON, and assign to a String
//    		String messageBody = gson.toJson(autoRegister);
//
//    		HTTPAuxiliarMethods httpAuxiliarMethods = new HTTPAuxiliarMethods();
//
//    		HttpResponse httpResponse = httpAuxiliarMethods.doHTTPPOSTRequest(address, messageBody, req.getHeader("Cookie"));
//
//    		httpAuxiliarMethods.closeClient();
    		if (autoRegister == null) {
    			model.addAttribute("message", "Failed");
    			model.addAttribute("messageNewUser", "User already exists!");
    			return autoRegisterPage(model, req);
    		}
    		 
    		model.addAttribute("message","Success");
            return autoRegisterPage(model, req);
            
        }
        model.addAttribute("message","Failed");
        return autoRegisterPage(model, req);
    }
}
