package br.inatel.dashboard.controller;

import static br.inatel.dashboard.constants.Constants.DASHBOARD_URL;
import static br.inatel.dashboard.constants.Constants.DELETE_DASHBOARD_URL;
import static br.inatel.dashboard.constants.Constants.LIST_USERS_URL;
import static br.inatel.dashboard.constants.Constants.NEW_DASHBOARD_URL;
import static br.inatel.dashboard.constants.Constants.NEW_WIDGET_URL;
import static br.inatel.dashboard.constants.Constants.PREFIX;
import static br.inatel.dashboard.constants.Constants.PUBLIC_DASHBOARD_URL;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.inatel.dashboard.Feign.Interface.AdminAPIProxyEureka;
import br.inatel.dashboard.Feign.Interface.AdminAPIProxyNoEureka;
import br.inatel.dashboard.model.Dashboard;
import br.inatel.dashboard.model.DashboardList;
import br.inatel.dashboard.model.DeviceList;
import br.inatel.dashboard.model.UserList;
import br.inatel.dashboard.model.Widget;
import br.inatel.dashboard.model.WidgetList;



@Controller
public class DashboardActionsController {

	@Autowired
	private AdminAPIProxyEureka adminAPIProxy;
	
	@Autowired
	private AdminAPIProxyNoEureka adminAPIProxy2;
	


	@RequestMapping(value = LIST_USERS_URL, method = RequestMethod.GET)
	public String listUsers(Model model, HttpServletRequest req) {
		
		String token = req.getHeader("Cookie").substring(10);
		DashboardList dashboardList;
		if(PREFIX.length()>1)
			dashboardList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashboardList = adminAPIProxy2.getDashboardsFromuser(token);
 
		UserList users;// = adminAPIProxy.listOtherUsers(token);
		
		if(PREFIX.length()>1)
			users = adminAPIProxy.listOtherUsers(token);
		else
			users = adminAPIProxy2.listOtherUsers(token);
		
		model.addAttribute("dashboards", dashboardList.getDashboards());
		model.addAttribute("users", users.getUsers());
		return "listUsers";
	}
	
	@RequestMapping(value = "/modifiedDashboard", method = RequestMethod.GET)
	public String modifiedDashboard() {

		return "modifiedDashboard";
	}
	
	public Optional<Dashboard> containsName(final List<Dashboard> list, final String name){
	    return list.stream().filter(o -> o.getId().equals(name)).findFirst();//.isPresent();
	}

	@RequestMapping(value = DASHBOARD_URL+"/{dashboardId}", method = RequestMethod.GET)
	public String dashboard(
			@PathVariable(name = "dashboardId", required = true) String dashboardId, Model model, HttpServletRequest req) {

		String token = req.getHeader("Cookie").substring(10);
		DashboardList dashboardList;
		if(PREFIX.length()>1)
			dashboardList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashboardList = adminAPIProxy2.getDashboardsFromuser(token);

		model.addAttribute("dashboards", dashboardList.getDashboards());
		if(dashboardId.length()<23) {
			return "dashboard";
		}

		Optional<Dashboard> dashboard = containsName(dashboardList.getDashboards(), dashboardId);
		
		
		if(!dashboard.isPresent()) {
			return "dashboard";
		}
		
		model.addAttribute("currentDashboard", dashboard.get());
		
		WidgetList widgetList;
		
		if(PREFIX.length()>1)
			widgetList = adminAPIProxy.getDashboardDetails(token, dashboardId);
		else
			widgetList = adminAPIProxy2.getDashboardDetails(token, dashboardId);
		

		Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ArrayList<Date> nvaloresArray = new ArrayList<Date>();
		
		
		
		for(int i = 0; i< widgetList.getWidgetList().size(); i++) {
//			String variable = widgetList.getWidgetList().get(i).getVariable();
			
			if(widgetList.getWidgetList().get(i).getGroup().isEmpty()) {
				nvaloresArray.add(widgetList.getWidgetList().get(i).getDevices().get(0).getMessages().get(0).getCreated_at());
				
			} else {
				
				List<String> typeList = new ArrayList<>();
				typeList.add("group");
				widgetList.getWidgetList().get(i).setTypeList(typeList );
				//widgetList.getWidgetList().get(i).getTypeList().add("group");
				//widgetList.getWidgetList().get(i).getTypeList().set(0, "group");
				nvaloresArray.add(widgetList.getWidgetList().get(i).getDevices().get(0).getCreateDate());
				
				//System.out.println("passou aqui " + widgetList.getWidgetList().get(i).getLatitudeCenter());

				
			}
			
			
			
			//nvaloresArray.add(widgetList.getWidgetList().get(i).getMessages().get(0).getCreated_at());
		}


		model.addAttribute("formatter", formatter);
		model.addAttribute("nvaloresArray", nvaloresArray);
		model.addAttribute("widgets", widgetList.getWidgetList());
		
		
		return "dinamicDashboard";
	}
	
	
	@RequestMapping(value = PUBLIC_DASHBOARD_URL+"/{dashboardId}", method = RequestMethod.GET)
	public String publicdashboard(
			@PathVariable(name = "dashboardId", required = true) String dashboardId, Model model, HttpServletRequest req) {

		DashboardList dashboardList;
		String token = req.getHeader("Cookie");
		
		if(token != null) {
			token = token.substring(10);
			if(PREFIX.length()>1)
				dashboardList = adminAPIProxy.getDashboardsFromuser(token);
			else
				dashboardList = adminAPIProxy2.getDashboardsFromuser(token);

			model.addAttribute("dashboards", dashboardList.getDashboards());
		}
		
		
		
		if(dashboardId.length()<23) {
			return "dashboard";
		}

		Dashboard dashboard = new Dashboard();
		dashboard.setName(dashboardId);
		model.addAttribute("currentDashboard", dashboard);
		
		WidgetList widgetList;
		
		if(PREFIX.length()>1)
			widgetList = adminAPIProxy.getPublicDashboardDetails(dashboardId);
		else
			widgetList = adminAPIProxy2.getPublicDashboardDetails(dashboardId);
		

		Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ArrayList<Date> nvaloresArray = new ArrayList<Date>();
		
		for(int i = 0; i< widgetList.getWidgetList().size(); i++) {
//			String variable = widgetList.getWidgetList().get(i).getVariable();
			
			nvaloresArray.add(widgetList.getWidgetList().get(i).getDevices().get(0).getMessages().get(0).getCreated_at());
			
			//nvaloresArray.add(widgetList.getWidgetList().get(i).getMessages().get(0).getCreated_at());
		}


		model.addAttribute("formatter", formatter);
		model.addAttribute("nvaloresArray", nvaloresArray);
		model.addAttribute("widgets", widgetList.getWidgetList());
		
		

		return "dinamicDashboard";
	}
	
	
	@RequestMapping(value = DASHBOARD_URL, method = RequestMethod.GET)
	public String demoDashboard(Model model, @RequestHeader("Cookie") String token2) {
		String token = token2.substring(10);
		DashboardList dashboardList;
		if(PREFIX.length()>1)
			dashboardList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashboardList = adminAPIProxy2.getDashboardsFromuser(token);
		
		//DashboardList

		model.addAttribute("dashboards", dashboardList.getDashboards());
		
		return "dashboard";
	}
	
	@RequestMapping(value = "dummyDash", method = RequestMethod.GET)
	public ModelAndView dummyDash() {
		 
		//userService.listU(auth.getName());
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.setViewName("dummyDash");
		return modelAndView;
	}

	@RequestMapping(value = NEW_DASHBOARD_URL, method = RequestMethod.GET)
	public String newDashboardForm(Model model, HttpServletRequest req) {
		
		String token = req.getHeader("Cookie").substring(10);
		DashboardList dashboardList;
		if(PREFIX.length()>1)
			dashboardList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashboardList = adminAPIProxy2.getDashboardsFromuser(token);

		model.addAttribute("dashboards", dashboardList.getDashboards());
		return "newDashboard";
	}
	
	@RequestMapping(value = DELETE_DASHBOARD_URL+"/{dashboardId}", method = RequestMethod.GET)
	public String deleteDashboard(Model model, 
			@PathVariable(name = "dashboardId", required = true) String dashboardId,
			HttpServletRequest req) {
		
		String token = req.getHeader("Cookie").substring(10);
		
		if(PREFIX.length()>1)
			adminAPIProxy.deleteDashboard(token, dashboardId);
		else
			adminAPIProxy2.deleteDashboard(token, dashboardId);
		
		DashboardList dashboardList;

		if(PREFIX.length()>1)
			dashboardList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashboardList = adminAPIProxy2.getDashboardsFromuser(token);

		model.addAttribute("dashboards", dashboardList.getDashboards());
		return "dashboard";
	}
	
	
	@RequestMapping(value = NEW_WIDGET_URL+"/{dashboardId}", method = RequestMethod.POST)
    public String insertWidgetboard(Model model, 
    		@Valid Widget widget, 
    		@ModelAttribute(value="type") String typeLight,
    		@ModelAttribute(value="group") String group,
    		@ModelAttribute(value="latitudeCenter") String latitudeCenter,
    		@ModelAttribute(value="longitudeCenter") String longitudeCenter,
    		@ModelAttribute(value="variable2") String selectVariables2,
    		@PathVariable(name = "dashboardId", required = true) String dashboardId, HttpServletRequest req) {
		
		
		
		
		if(typeLight.equalsIgnoreCase("light")) {
			
			widget.setGroup(group);
			widget.setLatitudeCenter(latitudeCenter);
			widget.setLongitudeCenter(longitudeCenter);

		} 
		else if(widget.getVariable().equals("no variables to add")) {
			//dashboard.setBelongsTo(auth.getName());
	        model.addAttribute("message","Failed");
	        //dashboardService.save(dashboard);
	        return "newWidget"; 
		}
		
		
		
		widget.setDashboardId(dashboardId);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        widget.setUsername(auth.getName());
        
        widget.setHeight("400");
        widget.setWidth("400");
        

        String token = req.getHeader("Cookie").substring(10);
		Widget widgetSent;// = adminAPIProxy.addWidget(token, widget);
		
		if(PREFIX.length()>1)
			widgetSent = adminAPIProxy.addWidget(token, widget, dashboardId);
		else
			widgetSent = adminAPIProxy2.addWidget(token, widget, dashboardId);


		
        model.addAttribute("message","Success");
        //dashboardService.save(dashboard);
        return newWidgetForm(model, dashboardId, req);
    }
    
	

	
	
	@RequestMapping(value = NEW_WIDGET_URL+"/{dashboardId}", method = RequestMethod.GET)
	public String newWidgetForm(Model model,
			@PathVariable(name = "dashboardId", required = true) String dashboardId, HttpServletRequest req) {
		
		String token = req.getHeader("Cookie").substring(10);
		DashboardList dashboardList;
		if(PREFIX.length()>1)
			dashboardList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashboardList = adminAPIProxy2.getDashboardsFromuser(token);

		Boolean isDashboardFromUser = false;
		for(int i = 0; i<dashboardList.getDashboards().size(); i++) {


			if(dashboardList.getDashboards().get(i).getId().equals(dashboardId)) {
				isDashboardFromUser=true;
			}
			
		}
		if(!isDashboardFromUser)
			return "dashboard";
		
		model.addAttribute("dashboardId", dashboardId);

        DeviceList devices;
        if(PREFIX.length()>1)
        	devices = adminAPIProxy.listDevices(token);
		else
			devices = adminAPIProxy2.listDevices(token);
        
    
        model.addAttribute("dashboards", dashboardList.getDashboards());
        //model.addAttribute("deviceGambs", deviceGambs);
        model.addAttribute("devices", devices.getDevices());
		
		return "newWidget";
	}
	
	
	@RequestMapping(value = NEW_DASHBOARD_URL, method = RequestMethod.POST)
    public String insertNewDashboard(Model model, @Valid Dashboard dashboard, HttpServletRequest req) {
		
		String token = req.getHeader("Cookie").substring(10);

		//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		//dashboard.setBelongsTo(auth.getName());
		if(PREFIX.length()>1)
			dashboard = adminAPIProxy.newDashboard(token, dashboard);
		else
			dashboard = adminAPIProxy2.newDashboard(token, dashboard);
		
		
		if (dashboard == null) {
			model.addAttribute("message", "Failed");
			model.addAttribute("messageNewUser", "Dashboard creation failed!");
			return newDashboardForm(model, req);
		}

        model.addAttribute("message","Success");
        return newDashboardForm(model, req);
    }
	
	@RequestMapping(value = "mapSites", method = RequestMethod.GET)
	public String mapSites(Model model, HttpServletRequest req) {

		
		String token = req.getHeader("Cookie").substring(10);
		DashboardList dashboardList;
		if(PREFIX.length()>1)
			dashboardList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashboardList = adminAPIProxy2.getDashboardsFromuser(token);

		model.addAttribute("dashboards", dashboardList.getDashboards());
		return "MapSites";
	}

	
}