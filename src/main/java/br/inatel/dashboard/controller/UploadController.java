package br.inatel.dashboard.controller;

import static br.inatel.dashboard.constants.Constants.PREFIX;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.inatel.dashboard.Feign.Interface.AdminAPIProxyEureka;
import br.inatel.dashboard.Feign.Interface.AdminAPIProxyNoEureka;
import br.inatel.dashboard.model.DashboardList;

@Controller
public class UploadController {
	
	@Autowired
	private AdminAPIProxyEureka adminAPIProxy;
	
	@Autowired
	private AdminAPIProxyNoEureka adminAPIProxy2;

    @GetMapping("/upload")
    public String index(Model model, HttpServletRequest req) {
    	
    	String token = req.getHeader("Cookie").substring(10);
        
        DashboardList dashboardList;
		if(PREFIX.length()>1)
			dashboardList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashboardList = adminAPIProxy2.getDashboardsFromuser(token);
		
		model.addAttribute("dashboards", dashboardList.getDashboards());
        return "upload";
    }

    
    public File convert(MultipartFile file) throws IOException {
    	ObjectId id = new ObjectId();
		
		String application = id.toHexString() + ".ffs";
		
    	File convFile = new File(application);
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }
    
    @PostMapping("/upload") // //new annotation since 4.3
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes, 
                                   String model, 
                                   String softwareVersion,
                                   HttpServletRequest req) throws IOException {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:uploadStatus";
        }
        
        String token = req.getHeader("Cookie").substring(10);
        
        String URL = req.getRequestURL().toString();
        if(URL.contains("8100")) {
        	URL = URL.replace("8100", "8190");
        }
        else {
        	URL.replace("admin-gui", "admin-api");
        }
        
        File file4 = convert(file);
        ContentBody cbFile = new FileBody(file4);


        HttpEntity entity = MultipartEntityBuilder.create()
                .addPart("file", cbFile)
                .addTextBody("model", model)
                .addTextBody("softwareVersion", softwareVersion)
                .build();

		HttpPost request = new HttpPost(URL);
		request.addHeader("Authorization", token);
		request.setEntity(entity);
		
		HttpClient client = HttpClientBuilder.create().build();
		try {
			client.execute(request);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		file4.delete();
		
		

        return "/uploadStatus";
    }
    
    
    @RequestMapping(value = "/uploadStatus", method = RequestMethod.GET)
    public void retrieveFile(HttpServletResponse response, HttpServletRequest req) throws IOException{
    	
    	
    	
    	String token = req.getHeader("Cookie").substring(10);

    	String URL = req.getRequestURL().toString();
		HttpGet request = new HttpGet(URL);
		request.addHeader("Authorization", token);


		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response2 = client.execute(request);
		
		InputStream myInputStream = response2.getEntity().getContent();
		
		

        
        String filename = "file";
        
        long length = response2.getEntity().getContentLength();//data.getSize();
        response.setHeader("content-disposition", "attachment; filename=\""+filename+"\".bin");
        response.setContentType("application/octet-stream");
        response.setContentLength((int) length);
        response.setContentLengthLong(length);
        
        try {
			IOUtils.copy(myInputStream, response.getOutputStream());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        

    }


}