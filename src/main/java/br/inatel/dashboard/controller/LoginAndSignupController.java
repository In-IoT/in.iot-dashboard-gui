package br.inatel.dashboard.controller;

import static br.inatel.dashboard.constants.Constants.DASHBOARD_URL;
import static br.inatel.dashboard.constants.Constants.LOGOUT_URL;
import static br.inatel.dashboard.constants.Constants.PREFIX;
import static br.inatel.dashboard.constants.Constants.SIGN_IN_URL;
import static br.inatel.dashboard.constants.Constants.SIGN_UP_URL_ADDITIONAL_USER;
import static br.inatel.dashboard.constants.Constants.SIGN_UP_URL_FIRST_USER;
import static br.inatel.dashboard.security.SecurityConstants.EXPIRATION_TIME;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.inatel.dashboard.Feign.Interface.AdminAPIProxyEureka;
import br.inatel.dashboard.Feign.Interface.AdminAPIProxyNoEureka;
import br.inatel.dashboard.model.Credentials;
import br.inatel.dashboard.model.DashboardList;
import br.inatel.dashboard.model.User;
import br.inatel.dashboard.security.CookieUtil;
import feign.FeignException;

@Controller
public class LoginAndSignupController {
	
	@Autowired
	private AdminAPIProxyEureka adminAPIProxy;
	
	@Autowired
	private AdminAPIProxyNoEureka adminAPIProxy2;

	@RequestMapping(value = SIGN_IN_URL, method = RequestMethod.GET)
	public String login() {
		return "login";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String autoRegisterPage() {
		return "login";
	}

	@RequestMapping(value = LOGOUT_URL, method = RequestMethod.GET)
	public String logout(HttpServletResponse res) {

		CookieUtil.clear(res, "JWT-TOKEN");
		
		//Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		SecurityContextHolder.clearContext();

		return "login";
	}

	// This is for login
	@RequestMapping(value = DASHBOARD_URL, method = RequestMethod.POST)
	public String login(HttpServletRequest req, HttpServletResponse res, Model model) {

		Credentials credentials = new Credentials();
//		User user = new User();
//		user.setEmail(req.getParameter("username"));
//		user.setPassword(req.getParameter("password"));
		credentials.setEmail(req.getParameter("username"));
		credentials.setPassword(req.getParameter("password"));
		ResponseEntity<String> response;
		try {
			if(PREFIX.length()>1)
				response = adminAPIProxy.login(credentials);
			else {
				response = adminAPIProxy2.login(credentials);
			}
		} catch(FeignException ex) {
			res.setStatus(403);
			return "login";
		}
		
		
		
		//ResponseEntity<String> response = adminAPIProxy.login(credentials);
		//response.getStatusCode()
//		if (response.getStatusCodeValue() != 200) {
//			
//		}
		String username = credentials.getEmail();
		Integer validity = (int) (long) EXPIRATION_TIME;

		String token = response.getHeaders().getFirst("Authorization");

		CookieUtil.create(res, "JWT-TOKEN", token, false, validity, "domain");


		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(username, null,
				new ArrayList<>());
		JSONObject jsonObj = new JSONObject(response.getBody());
		String application = jsonObj.getString("application");
		authentication.setDetails(application);

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		DashboardList dashList;
		if(PREFIX.length()>1)
			dashList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashList = adminAPIProxy2.getDashboardsFromuser(token);
		//DashboardList dashList = adminAPIProxy.getDashboardsFromuser(token);
		
		model.addAttribute("dashboards", dashList.getDashboards());

		return "dashboard";

	}

	@RequestMapping(value = SIGN_UP_URL_FIRST_USER, method = RequestMethod.GET)
	public String signup(HttpServletResponse res) {

		CookieUtil.clear(res, "JWT-TOKEN");
		
 
		SecurityContextHolder.clearContext();
		return "signup";
	}

	@RequestMapping(value = SIGN_UP_URL_FIRST_USER, method = RequestMethod.POST)
	public ModelAndView createNewUser(HttpServletRequest req, @Valid User user) {

		ModelAndView modelAndView = new ModelAndView();
		
		//ResponseEntity<String> response;
		if(PREFIX.length()>1)
			adminAPIProxy.createNewUser(user);
		else
			adminAPIProxy2.createNewUser(user);
		//adminAPIProxy.createNewUser(user);
		//String address = "http://" + req.getLocalAddr() + ":" + req.getLocalPort() + SIGN_UP_URL_FIRST_USER_REST;
		//Gson gson = new Gson();

		// 2. Java object to JSON, and assign to a String
		//String messageBody = gson.toJson(user);

		//HTTPAuxiliarMethods httpAuxiliarMethods = new HTTPAuxiliarMethods();

		//HttpResponse httpResponse = httpAuxiliarMethods.doHTTPPOSTRequest(address, messageBody, null);
		//httpAuxiliarMethods.closeClient();	
		
		//if (httpResponse.getEntity().getContentLength() == 0) {
		//	modelAndView.setViewName("signup");
		//	return modelAndView;
		//}
		//if (newUser)
		
		modelAndView.addObject("successMessage", "User has been registered successfully");
		modelAndView.addObject("user", new User());
		modelAndView.setViewName("login");

		return modelAndView;
	}

	@RequestMapping(value = SIGN_UP_URL_ADDITIONAL_USER, method = RequestMethod.GET)
	public String newUser(Model model, HttpServletRequest req) {
		
		
		String token = req.getHeader("Cookie").substring(10);
		DashboardList dashboardList;
		if(PREFIX.length()>1)
			dashboardList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashboardList = adminAPIProxy2.getDashboardsFromuser(token);
//		
//		String address = "http://" + req.getLocalAddr() + ":" + req.getLocalPort() + GET_DASHBOARDS_FROM_USER_REST_URL;
//
//		HTTPAuxiliarMethods httpAuxiliarMethods = new HTTPAuxiliarMethods();
//
//		String dashboards  = httpAuxiliarMethods.getDashboardList(address, req.getHeader("Cookie"));
//		
//
//		ObjectMapper mapper = new ObjectMapper();
//		DashboardList dashboardList = new DashboardList();
//		try {
//			dashboardList = mapper.readValue(dashboards, DashboardList.class);
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		model.addAttribute("dashboards", dashboardList.getDashboards());
		return "newUser";
	}

	@RequestMapping(value = SIGN_UP_URL_ADDITIONAL_USER, method = RequestMethod.POST)
	public String createUserWhenLogged(@Valid User user, Model model, HttpServletRequest req) {

		
		String token = req.getHeader("Cookie").substring(10);
		
		if(PREFIX.length()>1)
			user = adminAPIProxy.createUserWhenLogged(token, user);
		else
			user = adminAPIProxy2.createUserWhenLogged(token, user);
		
		if (user == null) {
			model.addAttribute("message", "Failed");
			model.addAttribute("messageNewUser", "User already exists!");
			return newUser(model, req);
		}
		
		model.addAttribute("message", "Success");
		model.addAttribute("messageNewUser", "User sucessfully created!");

		return newUser(model, req);

	}

}