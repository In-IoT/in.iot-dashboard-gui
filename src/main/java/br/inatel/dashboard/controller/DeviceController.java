package br.inatel.dashboard.controller;

import static br.inatel.dashboard.constants.Constants.LIST_ACTIVE_MQTT_DEVICES_URL;
import static br.inatel.dashboard.constants.Constants.LIST_DEVICES_URL;
import static br.inatel.dashboard.constants.Constants.NEW_DEVICES_URL;
import static br.inatel.dashboard.constants.Constants.PREFIX;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import br.inatel.dashboard.Feign.Interface.AdminAPIProxyEureka;
import br.inatel.dashboard.Feign.Interface.AdminAPIProxyNoEureka;
import br.inatel.dashboard.controller.utils.HTTPAuxiliarMethods;
import br.inatel.dashboard.model.DashboardList;
import br.inatel.dashboard.model.Device;
import br.inatel.dashboard.model.DeviceList;
import br.inatel.dashboard.model.MQTTDeviceList;

@Controller
public class DeviceController {
	

    @Autowired
	private AdminAPIProxyEureka adminAPIProxy;
	
	@Autowired
	private AdminAPIProxyNoEureka adminAPIProxy2;

    @Autowired
    public DeviceController( 
    		BCryptPasswordEncoder passwordEncryptor) {
    }

    @RequestMapping(value = LIST_DEVICES_URL, method = RequestMethod.GET)
    public String listDevices(Model model, HttpServletRequest req) {
    	
    	String token = req.getHeader("Cookie").substring(10);
		DashboardList dashboardList;
		if(PREFIX.length()>1)
			dashboardList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashboardList = adminAPIProxy2.getDashboardsFromuser(token);

    	DeviceList deviceList;
    	if(PREFIX.length()>1)
    		deviceList = adminAPIProxy.listDevices(token);
		else
			deviceList = adminAPIProxy2.listDevices(token);
		
		model.addAttribute("dashboards", dashboardList.getDashboards());
        //List<Device> devices = deviceService.findAll();
        model.addAttribute("devices", deviceList.getDevices());

        return "listDevices";
    }
    
    @RequestMapping(value = LIST_DEVICES_URL, method = RequestMethod.POST)
    public String resetDevicePassword(Model model, Device device, HttpServletRequest req) {

    	String token = req.getHeader("Cookie").substring(10);
    	
    	if(PREFIX.length()>1)
    		device = adminAPIProxy.resetDevicePassword(token, device);
		else
			device = adminAPIProxy2.resetDevicePassword(token, device);
    	
    	if(device == null) {
    		model.addAttribute("message","Failed");
    		return listDevices(model, req);
    	}
    	
        model.addAttribute("message","Success");
        model.addAttribute("deviceUsername", device.getUsername());
        model.addAttribute("devicePassword", device.getPassword());
        model.addAttribute("deviceApplication", device.getApplication());
        return listDevices(model, req);
    }

    @RequestMapping(value = NEW_DEVICES_URL, method = RequestMethod.GET)
    public String newDevice(Model model, HttpServletRequest req) {

		String token = req.getHeader("Cookie").substring(10);
		DashboardList dashboardList;
		if(PREFIX.length()>1)
			dashboardList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashboardList = adminAPIProxy2.getDashboardsFromuser(token);
		
		model.addAttribute("dashboards", dashboardList.getDashboards());
		
		Device device = new Device();
		//["  "];
		List<String> abilities = new ArrayList<>();
		abilities.add("");
		device.setAbilities(abilities);
		device.setGroups(abilities);
		device.setVariables(abilities);
		model.addAttribute("device", device);

        return "newDevice";
    }

    @RequestMapping(value = NEW_DEVICES_URL, method = RequestMethod.POST)
    public String insertNewDevice(Model model, @Valid Device device, HttpServletRequest req) {
      
    	String token = req.getHeader("Cookie").substring(10);
    	
    	String group = req.getParameter("group").replace(" ", "");
    	String ability = req.getParameter("abilities").replace(" ", "");
    	String variable = req.getParameter("variable").replace(" ", "");
    	
    	List<String> groups = Arrays.asList(group.split(","));
    	List<String> abilities = Arrays.asList(ability.split(","));
    	List<String> variables = Arrays.asList(variable.split(","));
    	
    	String deviceId = device.getId();

    	    	    	
    	device.setGroups(groups);
    	
    	device.setAbilities(abilities);
    	
    	device.setVariables(variables);

    	
    	

    	if(PREFIX.length()>1)
			device = adminAPIProxy.createnewDevice(token, device);
		else
			device = adminAPIProxy2.createnewDevice(token, device);
    	
    	

    	if(deviceId.equals(device.getId())) {
    		model.addAttribute("message","Success2");
    		return newDevice(model, req);
    	}
        model.addAttribute("message","Success");
        model.addAttribute("deviceUsername", device.getUsername());
        model.addAttribute("devicePassword", device.getPassword());
        model.addAttribute("deviceApplication", device.getApplication());
        //model.addAttribute("devicePassword", device.getApplication());
        return newDevice(model, req);
        
    }
    
    
    @RequestMapping(value = "/edit/{deviceId}", method = RequestMethod.GET)
    public String editDevice(Model model, @Valid Device device, HttpServletRequest req,
    		@PathVariable(name = "deviceId", required = true) String deviceId) {
      
    	String token = req.getHeader("Cookie").substring(10);
    	
    	DashboardList dashboardList;
		if(PREFIX.length()>1)
			dashboardList = adminAPIProxy.getDashboardsFromuser(token);
		else
			dashboardList = adminAPIProxy2.getDashboardsFromuser(token);
		
		model.addAttribute("dashboards", dashboardList.getDashboards());

    	if(PREFIX.length()>1)
			device = adminAPIProxy.getDeviceDetails(token, deviceId);
		else
			device = adminAPIProxy2.getDeviceDetails(token, deviceId);
    	
    	if(device == null) {
    		device = new  Device();
//    		List<String> abilities = new ArrayList<>();
//    		abilities.add("");
//    		device.setAbilities(abilities);
//    		device.setGroups(abilities);
//    		device.setVariables(abilities);
    	}
    	List<String> abilities = new ArrayList<>();
    	abilities.add("");
    	
    	if(device.getAbilities() == null)
    		device.setAbilities(abilities);
    	if(device.getGroups() == null)
    		device.setGroups(abilities);
    	if(device.getVariables() == null)
    		device.setVariables(abilities);
    	//device.getAbilityDetails().
    	
    	if(device.getAbilityDetails() != null) {
    	Gson gson = new Gson();
        String uglyJSONString = gson.toJson(device.getAbilityDetails());
        
        gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(uglyJSONString);
        String prettyJsonString = gson.toJson(je);
        
        device.setAbilityDetails(prettyJsonString);
    	}
        
        

    	model.addAttribute("device", device);
//
//        model.addAttribute("message","Success");
//        model.addAttribute("deviceUsername", device.getUsername());
//        model.addAttribute("devicePassword", device.getPassword());
        return "newDevice";
    }
    
    
    
//    @RequestMapping(value = "/findDevicesByGroup" + "/{groupName}", method = RequestMethod.GET)
//	public String getDeviceByGroup(
//			@PathVariable(name = "groupName", required = true) String groupName, Model model, @RequestHeader(name="Cookie") String token) {
//		
//    	
//    	token = token.substring(10);
//    	
//    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//
//		List <Device> devices = deviceService.findByGroup(groupName);
//
//
//		
//
//		Response<List<Device>> response = new Response<>();
//		List<Message> messages;
//		for (int i = 0; i < devices.size(); i++) {
//			
//			 messages = messageService.findByUsername(devices.get(i).getUsername(), 30);
//			
//			 devices.get(i).setMessages(messages);
//
//		}
//
//		
//		response.setData(devices);
//
//
//		return ResponseEntity.ok(response);
//
//	}
    

    public String showNewDevice(Model model) {
        return "newDevice";
    }
    
    
    
    @RequestMapping(value = LIST_ACTIVE_MQTT_DEVICES_URL, method = RequestMethod.GET)
    public String listActiveMQTTDevices(Model model, HttpServletRequest req) {

    	
    	String address = "http://localhost:8239/listConnectedDevices";

		HTTPAuxiliarMethods httpAuxiliarMethods = new HTTPAuxiliarMethods();

		String str  = httpAuxiliarMethods.getRandomString(address, req.getHeader("Cookie").replace("JWT-TOKEN=", ""));

		
		ObjectMapper mapper = new ObjectMapper();
		
		MQTTDeviceList user = null;
		try {
			user = mapper.readValue(str, MQTTDeviceList.class);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		model.addAttribute("mqttdevices", user.getUsers());
		
		
        return "listActiveDevices";
    }


}
