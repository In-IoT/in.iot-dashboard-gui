package br.inatel.dashboard;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients

public class RunAdminGUI extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(RunAdminGUI.class);
	}

	public static void main(String[] args) {
		checkIfConfigFileExists();
		SpringApplication.run(RunAdminGUI.class, args);
	}
	
	private static void checkIfConfigFileExists() {

		String filename = System.getProperty("user.dir")+ File.separator + "config" + File.separator + "application.properties";
		File file = new File(filename);
		if (!file.exists()) {
			file.mkdirs();
			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try (Writer writer = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(filename), "utf-8"))) {
				writer.write(""
						+ "#logging.level.org.springframework.web: WARN\n" + 
						"#logging.level.org.hibernate: WARN\n" + 
						"\n" + 
						"server.port = 8100\n" + 
						"spring.application.name= admin-gui\n" + 
						"spring.cloud.refresh.enabled=false\n" + 
						"  \n" + 
						"eureka.client.registerWithEureka=false\n" + 
						"#eureka.client.fetchRegistry=true\n" + 
						"eureka.client.serviceUrl.defaultZone=http://inIoTTest:lucasabbadeWare@localhost:8761/eureka/\n" + 
						"eureka.instance.preferIpAddress= true\n" + 
						"\n" + 
						"\n" + 
						"#If eureka and Zuul are disabled, these two options should be set to false\n" + 
						"eureka.client.register-with-eureka=false\n" + 
						"eureka.client.fetch-registry=false\n" + 
						"\n" + 
						"\n" + 
						"#example of mongoDB Running on localhost in port 27017 and database iniot\n" + 
						"#MONGODBURL = mongodb://localhost:27017\n" + 
						"#MONGODATABASE = iniot\n" + 
						"\n" + 
						"\n" + 
						"#example of mongoDB Running on mongodb cloud server and database iniot\n" + 
						"#mongodb+srv://iotlab:316S7HKjvFXiSHkvdWiAee@cluster0-065g.mongodb.net\n" + 
						"#MONGODATABASE = iniot"
						+ "");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Configuration
	class RestTemplateConfig {
		
		// Create a bean for restTemplate to call services
		@Bean
		@LoadBalanced		// Load balance between service instances running at different ports.
		public RestTemplate restTemplate() {
		    return new RestTemplate();
		}
	}
}

//@SpringBootApplication
//public class RunAPI {
//
//	public static void main(String[] args) {
//		SpringApplication.run(RunAPI.class, args);
//	}
//}
