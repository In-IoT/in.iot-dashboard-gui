package br.inatel.dashboard.common;

public class ErrorMessages {

    public static final String USER_NOT_FOUND = "User not found!";
    public static final String USER_ALREADY_EXISTS = "User already exists";
    public static final String WRONG_CREDENTIALS = "Wrong username or password!";
    public static final String INVALID_TOKEN = "Invalid authentication token!";
    public static final String MESSAGE_NOT_FOUND = "Message not found!";
    public static final String NO_CREDENTIALS_PROVIDED = "No Credentials were provided";
    public static final String NO_VARIABLES_PROVIDED = "No data variables were provided";
    
}
